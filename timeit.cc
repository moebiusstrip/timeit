#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

struct list{
	int msec;
	char *line;
	int rc;
	list *next;
};

list *lhead = NULL;
list *lnext = NULL;

void addlist(int time, const char *line, int rc){
	list *lnew = (list *) malloc(sizeof(list));
	if(lnew == NULL){
		perror("Bad things happened (could not allocate memory)");
		exit(7);
	}
	if(lhead == NULL) lhead = lnew;
	if(lnext == NULL) lnext = lnew;
	else{
		lnext->next = lnew;
		lnext = lnext->next;
	}
	lnext->next = NULL;
	lnext->msec = time;
	lnext->rc   = rc;
	lnext->line = strdup( line);
}

void freelist(){
	while(lhead != NULL){
		lnext = lhead;
		lhead = lnext->next;
		if(lnext->line != NULL) free(lnext->line);
		free(lnext);
	}
}

int sumtime(){
	lnext = lhead;
	int ret = 0;
	while( lnext != NULL){
		ret += lnext->msec;
		lnext = lnext->next;
	}
	return ret;
}

void timepp(int time, FILE*out=NULL){
	int sec = time/1000;
	int msec = time - (sec*1000);
	int min = sec/60;
	sec -= min*60;
	int hour = min/60;
	min -= hour*60;
	int day = hour/24;
	hour -= day*24;
	char buffer[256];
	buffer[0] = 0x00;
	if( day > 0)  sprintf(buffer, "%s%dd ", buffer, day);
	if( hour > 0) sprintf(buffer, "%s%2dh ", buffer, hour);
	if( min > 0)  sprintf(buffer, "%s%2dm ", buffer, min);
	if( sec > 0)  sprintf(buffer, "%s%2ds ", buffer, sec);
	if( msec > 0) sprintf(buffer, "%s%3dms", buffer, msec);
    if( time == 0) sprintf(buffer, "0ms");
	int i;
    if( out == NULL){
    	for(i=23-strlen(buffer); i > 0; i--)printf(" ");
    	printf("%s", buffer);
    } else {
        fprintf(out, "\"%s\"", buffer);
    }
}

void showlist(){
	float tmsec = sumtime();
	lnext = lhead;
	printf("overall%%\t           time\t\t\t timepp\t     rc\tcommand\n"/*, lnext->msec, lnext->rc, lnext->line*/);
	while( lnext != NULL){
		printf("%6.2f%% \t%15d\t",(lnext->msec/tmsec)*100,lnext->msec);
		timepp(lnext->msec);
		printf("\t%7d\t%s\n", lnext->rc, lnext->line);
		lnext = lnext->next;
	}
	printf("========================================================\n");
	printf("100.00%% \t%15d\t", (int)tmsec);
	timepp(tmsec);
	printf("\n");
}

void escapeprint(const char * const line, FILE* out){
    int i = 0;
    while( line[i] != 0x00){
        if( line[i] == '"') putc('"', out);
        putc(line[i], out);
        i++;
    }
}

void writelist(FILE *csv){
   	float tmsec = sumtime();
	lnext = lhead;
	fprintf(csv,"\"overall%%\",\"time[ms]\",\"timepp\",\"rc\",\"command\"\n");
	while( lnext != NULL){
		fprintf(csv,"\"%6.2f%%\",%8d,",(lnext->msec/tmsec)*100,lnext->msec);
		timepp(lnext->msec, csv);
		fprintf(csv,",%4d,\"", lnext->rc);
        escapeprint( lnext->line, csv);
        fprintf(csv,"\"\n");
		lnext = lnext->next;
	}
}

int timesub(timeval start, timeval stop){
	int msec = stop.tv_sec - start.tv_sec;
	int msecu = stop.tv_usec - start.tv_usec;
	if(msecu < 0){
		msecu += 1000000;
		msec -= 1;
	}

	msecu+=500;
	msecu /= 1000;
	msec *= 1000;
	return (msec+msecu);
}

void run(const char *line){
	timeval start, stop;
	gettimeofday(&start, NULL);
	int rc = system( line);
	gettimeofday(&stop, NULL);
	if( rc == -1){
		printf("Error while executing command '%s'\n", line);
		perror(line);
		return;
	}
#ifdef __unix__
	rc = WEXITSTATUS(rc);
#else
    rc = (unsigned int) rc;
#endif
	if( rc == 127){
		printf("Return code is 127, this could indicate that the shell could not be executed.\n");
	}
	int msec = timesub(start, stop);
	addlist(msec, line, rc);
}


#if !defined(__unix__)
/* read till line feed 0x0a, exclude it*/
int getline( char **buffer_, size_t *len_, FILE *stream){
    int ret = 0;
    int len = *len_;
    char *buffer = *buffer_; //template stuff
    unsigned char last = 0x00;
    int read;
    while( (read = getc(stream)) != EOF){
        last = (unsigned char) read;
        if( last == 0x0a) break;
        buffer[ret++] = last;
        if (ret == len-1) break;
    }
    buffer[ret]=0x00; //delimit string
    if( read == EOF && ret == 0) ret = -1;
    if( read == EOF) return ret; //done
    if( last != 0x0a) fprintf(stderr, "EE    Buffer to small, skipping rest of line\n");
    while( last != 0x0a){ //skip forward to next line
        read = getc(stream);
        if( read == EOF) last =0x0a;
        else last = (unsigned int) read;
    }
    return ret;
}
#endif



int main(int argc, char **argv)
{
	int r = system(NULL);
	switch(r){
		case -1: printf("Bad things happened (fork failed on system(3) call.\n"); exit(1); break;
		case  0: printf("Bad things happened (no shell found for system(3) call.\n)"); exit(2); break;
	}

	if( argc <2){
//		printf("Where is the command file?\n");
		printf("%s <input file> <output.csv>\n",argv[0]);
		printf("\tThe input file should contain one command per line.\n");
		printf("\tEach command will be executed using the shell (so a\n");
		printf("\trun of '/bin/sh command' should work, this can be  \n");
		printf("\ttested easily by running '/bin/sh <input file>').\n");
        printf("\t<output.csv> is optional.\n");
		exit(3);
	}
    FILE *csv = NULL;
    if( argc > 2){
        csv = fopen(argv[2], "w");
        if(csv == NULL){
            perror("Couldn't open file for writing");
        }
    }


	FILE *fh;
	fh = fopen(argv[1], "r");
	if(fh == NULL){
		perror("Couldn't open file for reading");
		exit(4);
	}

#ifdef __unix__
	char *buffer=NULL;
	size_t len = 0;
#else
    size_t len = 1024;
    char *buffer = (char *)malloc(sizeof(char)*len);
    if( buffer == NULL){
        fprintf(stderr, "EE    Could not allocate buffer for reading commands of size %d\n", (int) sizeof(char)*len);
        exit(-1);   
    };
#endif
	int i = 0;
	int count = 0;
	while( (i = getline(&buffer, &len, fh)) >= 0){
        while( i > 0 && (buffer[i-1] == 0x0a || buffer[i-1] == 0x0d) ) buffer[--i] = 0x00;
		if( i == 0) continue;
		printf("line %d: %s\n", count++, buffer);
        fflush(stdout);
		run(buffer);
	}
	if(buffer) free(buffer);
	showlist();
    if( csv != NULL){
        writelist(csv);
        fclose(csv);
    }
    fclose(fh);
	freelist();
	return 0;
}
